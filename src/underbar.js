(function() {

  'use strict';

  window._ = {};



  _.identity = function(val) {

    return val;

  };


  _.first = function(array, n) {

    return n === undefined ? array[0] : array.slice(0, n);

  };


  _.last = function(array, n) {

    if (n === 0) {

      return [];

    }

    return n === undefined ? array[array.length - 1] : array.slice(-n);

  };


  _.each = function(collection, iterator) {

    if (Array.isArray(collection)) {

      for (var i = 0; i < collection.length; i++) {

        iterator(collection[i], i, collection)

      }

    } else if (typeof collection === 'object') {

      for (var key in collection) {

        iterator(collection[key], key, collection)

      }

    }

  };


  _.indexOf = function(array, target){

    var result = -1;

    _.each(array, function(item, index) {

      if (item === target && result === -1) {

        result = index;

      }

    });

    return result;

  };


  _.filter = function(collection, test) {

    var res = [];

    _.each(collection, function(el) {

      if (test(el)) {

        res.push(el);

      }

    });

    return res;

  };


  _.reject = function(collection, test) {

    var res = [];

    _.each(collection, function(el) {

      if (!test(el)) {

        res.push(el);

      }

    });

    return res;

  };


  _.uniq = function(array, isSorted, iterator) {

    var resArr = [];

    var uniqCheck = {};

    if (!iterator) {

      iterator = _.identity;

    }

    _.each(array, function(el) {

      if (!uniqCheck.hasOwnProperty(iterator(el))) {

        resArr.push(el);

        uniqCheck[iterator(el)] = el;

      }

    });

    return resArr;

  };


  _.map = function(collection, iterator) {

    var resArr = [];

    _.each(collection, function(el) {

      resArr.push(iterator(el));

    });

    return resArr;

  };


  _.pluck = function(collection, key) {

    return _.map(collection, function(item){

      return item[key];

    });

  };


  _.reduce = function(collection, iterator, accumulator) {

    var accNotPassed = (accumulator === undefined);

    _.each(collection, function(item) {

      if (accNotPassed) {

        accumulator = item;

        accNotPassed = false;

      } else {

        accumulator = iterator(accumulator, item);

      }

    });

    return accumulator;

  };


  _.contains = function(collection, target) {

    return _.reduce(collection, function(wasFound, item) {

      if (wasFound) {

        return true;

      }

      return item === target;

    }, false);

  };


  _.every = function(collection, iterator) {

    if (collection.length === 0) {

      return true;

    } else if (!iterator) {

      iterator = _.identity;

    }

    var conditionalAcc = true;

    return _.reduce(collection, function(conditionalAcc, el) {

      if (conditionalAcc !== true) {

        return false;

      } else {

        return Boolean((iterator(el)));

      }

    }, conditionalAcc);

  };


  _.some = function(collection, iterator) {

    if (collection.length === 0) {

      return false;

    } else if (!iterator) {

      iterator = _.identity;

    }

    var conditionalAcc = false;

    return _.reduce(collection, function(conditionalAcc, el) {

      if (conditionalAcc !== false) {

        return true;

      } else {

        return Boolean((iterator(el)));

      }

    }, conditionalAcc);

  };


  _.extend = function(obj) {

    var donors = Array.from(arguments);

    var recepient = donors.shift();

    _.each(donors, function(el) {

      var enumParams = Object.keys(el);

      for (var i = 0; i < enumParams.length; i++) {

        recepient[enumParams[i]] = el[enumParams[i]];

      }

    });

    return recepient;

  };


  _.defaults = function(obj) {

    var donors = Array.from(arguments);

    var recepient = donors.shift();

    _.each(donors, function(el) {

      var enumParams = Object.keys(el);

      for (var i = 0; i < enumParams.length; i++) {

        if (!recepient.hasOwnProperty(enumParams[i])) {

          recepient[enumParams[i]] = el[enumParams[i]];

        }

      }

    });

    return recepient;

  };


  _.once = function(func) {

    var alreadyCalled = false;

    var result;

    return function() {

      if (!alreadyCalled) {

        result = func.apply(this, arguments);

        alreadyCalled = true;

      }

      return result;

    };

  };


  _.memoize = function(func) {

    var testObj = {};

    return function() {

      var args = Array.from(arguments);

      for (var i = 0; i < args.length; i++) {

        if (testObj[args[i]]) {

          return testObj[args];

        } else {

          return (testObj[args] = func.apply(null, args));

        }

      }

    }

  };


  _.delay = function(func, wait) {

    var args = Array.from(arguments).slice(2);

    return setTimeout(function() {

      func.apply(null,args);

    }, wait);

  };


  _.shuffle = function(array) {

    var newArr = array.slice();

    var length = newArr.length - 1;

    function getRandomIntInclusive(min, max) {

      min = Math.ceil(min);

      max = Math.floor(max);

      return Math.floor(Math.random() * (max - min + 1)) + min;

    }

    for (var i = 0; i < length; i++) {

      var rndI = getRandomIntInclusive(0, length);

      var indexKeeper = newArr[i];

      newArr[i] = newArr[rndI];

      newArr[rndI] = indexKeeper;

    }

    return newArr;

  };


  /*
    Didn't format below corresspondigly so as to leave comment structure etc. in place for finishing up after submission of requirements.
  */


  /**
   * ADVANCED
   * =================
   *
   * Note: This is the end of the pre-course curriculum. Feel free to continue,
   * but nothing beyond here is required.
   */

  // Calls the method named by functionOrKey on each value in the list.
  // Note: You will need to learn a bit about .apply to complete this.
  _.invoke = function(collection, functionOrKey, args) {
  };

  // Sort the object's values by a criterion produced by an iterator.
  // If iterator is a string, sort objects by that property with the name
  // of that string. For example, _.sortBy(people, 'name') should sort
  // an array of people by their name.
  _.sortBy = function(collection, iterator) {
  };

  // Zip together two or more arrays with elements of the same index
  // going together.
  //
  // Example:
  // _.zip(['a','b','c','d'], [1,2,3]) returns [['a',1], ['b',2], ['c',3], ['d',undefined]]
  _.zip = function() {
  };

  // Takes a multidimensional array and converts it to a one-dimensional array.
  // The new array should contain all elements of the multidimensional array.
  //
  // Hint: Use Array.isArray to check if something is an array
  _.flatten = function(nestedArray, result) {
  };

  // Takes an arbitrary number of arrays and produces an array that contains
  // every item shared between all the passed-in arrays.
  _.intersection = function() {
  };

  // Take the difference between one array and a number of other arrays.
  // Only the elements present in just the first array will remain.
  _.difference = function(array) {
  };

  // Returns a function, that, when invoked, will only be triggered at most once
  // during a given window of time.  See the Underbar readme for extra details
  // on this function.
  //
  // Note: This is difficult! It may take a while to implement.
  _.throttle = function(func, wait) {
  };
}());
